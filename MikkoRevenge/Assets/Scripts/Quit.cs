﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Quit : MonoBehaviour {

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKey(KeyCode.Return))
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene("Main");
        }
    }   
}
