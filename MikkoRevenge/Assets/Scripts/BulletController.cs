﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float speed;

    public PlayerMovement player;




    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();

        if (Input.GetAxisRaw("Horizontal") < -0.5f)
            speed = -speed;
    }

    // Update is called once per frame
    void Update () {
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.tag == "Enemy")
        {
            Destroy(coll.gameObject);
        }


        Destroy(gameObject);
    }

    }