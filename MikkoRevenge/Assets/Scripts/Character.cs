﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    //Movement Variables
    private float movex;
    private float movey;
    public float moveSpeed;


    //Update is called once per frame
    void FixedUpdate()
    {

        movex = Input.GetAxis("Horizontal");
        movey = Input.GetAxis("Vertical");

        GetComponent<Rigidbody2D>().velocity = new Vector2(movex * moveSpeed, movey * moveSpeed);
    }
}